
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author pacio
 */
public class Danych {
    private static Statement s;
    private static Connection c;
    private static String url="jdbc:mysql://localhost:3306/sys";
    private static String username = "root";
    private static String password = "haslo";
    
       public static void polacz() throws ClassNotFoundException{
       
        try {
            Class.forName("com.mysql.jdbc.Driver");
             c = DriverManager.getConnection(url,username,password);
             s=c.createStatement();
        
        } catch (SQLException ex) {
            Logger.getLogger(Baza.class.getName()).log(Level.SEVERE, null, ex);
        }
       
        

    }
       public static void koniec() throws SQLException{
           s.close();
           c.close();
       }
       public static Statement getStatment() throws ClassNotFoundException{
           if(s == null){
               polacz();
           }
           return s;
       }
    
}
