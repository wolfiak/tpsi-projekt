
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author pacio
 */
@ManagedBean(name="aw")
@SessionScoped
public class AutorW {
    private static int wybor;
    private  Autor opisaa;
    private Autor[] tab=Baza.getAutorowie();

    public static int getWybor() {
        return wybor;
    }

    public static void setWybor(int wybor) {
        AutorW.wybor = wybor;
    }

    public Autor getOpisaa() {
        Autor tmp=null;
        for(int n=0;n<tab.length;n++){
            if(tab[n].getId()==wybor){
                tmp=tab[n];
            }
        }
        return tmp;
    }

    public void setOpisaa(Autor opisaa) {
        this.opisaa = opisaa;
    }
    
    
}
