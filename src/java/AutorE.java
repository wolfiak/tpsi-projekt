
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author pacio
 */
@ManagedBean(name="autore")
@SessionScoped
public class AutorE {
        private int id;
        private String imie;
        private String nazwisko;
        private String url;
        private String opis;
        
            public String dodaj() throws SQLException{
         try {
            Statement s=Danych.getStatment();
            s.execute("DELETE FROM autor WHERE id="+id+"");
            int lol = s.executeUpdate("INSERT INTO autor (id,imie,nazwisko,url,opis) VALUES ('"+id+"','"+imie+"','"+nazwisko+"','"+url+"','"+opis+"') ");
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(Rejestracja.class.getName()).log(Level.SEVERE, null, ex);
        }
        return "/panel";
    }
        public String odbierz(){
         Map<String, String> parameterMap = (Map<String, String>) FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
             int tmp = Integer.parseInt(parameterMap.get("id"));
             ListaAutor a= Panel.getA();
             Autor tmpa=null;
             for(int n=0;n<a.size();n++){
                 if(a.get(n).getId()==tmp){
                     tmpa=a.get(n);
                 }
                 
             }
             id=tmpa.getId();
             imie=tmpa.getImie();
             nazwisko=tmpa.getNaziwsko();
             url=tmpa.getUrl();
             opis=tmpa.getOpis();
           
             return "/edycjaaut";
    }
     public String usun() throws ClassNotFoundException, SQLException{
          Map<String, String> parameterMap = (Map<String, String>) FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
          int tmp = Integer.parseInt(parameterMap.get("id"));
          Statement s=Danych.getStatment();
          s.execute("DELETE FROM autor WHERE id="+tmp+"");   
          return "autore"; 
     }
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getImie() {
        return imie;
    }

    public void setImie(String imie) {
        this.imie = imie;
    }

    public String getNazwisko() {
        return nazwisko;
    }

    public void setNazwisko(String nazwisko) {
        this.nazwisko = nazwisko;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getOpis() {
        return opis;
    }

    public void setOpis(String opis) {
        this.opis = opis;
    }
        
        
}
