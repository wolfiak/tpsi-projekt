
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author pacio
 */
public class ListaWydawnictwo {
      private List<Wydawnictwo> k=new ArrayList<Wydawnictwo>();
      
      
      public   void add(Wydawnictwo ks){
          k.add(ks);
      }
      public  Wydawnictwo get(int index){
          return k.get(index);
      }
      public void clear(){
          k.clear();
      }
      public Wydawnictwo find(int id){
          Wydawnictwo ko = null;
          for(int n=0;n<k.size();n++){
              if(k.get(n).getId()==id){
                  ko=k.get(n);
              }
          }
          return ko;
      }
      public int size(){
         return k.size();
      }

    public List<Wydawnictwo> getK() {
        return k;
    }

    public void setK(List<Wydawnictwo> k) {
        this.k = k;
    }
      
}
