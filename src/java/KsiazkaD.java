
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author pacio
 */
@ManagedBean(name="ksiazkad")
@SessionScoped
public class KsiazkaD {
    private String tytul;
    private String url;
    private int liczba_stron;
    private int cena;
    private int ilosc;
   private String wydanie;
   private int id_autora;
   private int id_wydawnictwa;
   private static Map<String,String> wyd=new LinkedHashMap<String,String>();
   private static ListaWydawnictwo w=new ListaWydawnictwo();
   private static Map<String,String> aut=new LinkedHashMap<String,String>();
   private static ListaAutor a=new ListaAutor();
   private static String test;
   private String test2;
   private static String test3;
   private static String tratest2;
   
   private  String test4;
    public static String getTest() {
        return test;
    }

    public static void setTest(String test) {
        KsiazkaD.test = test;
    }

    public String getTest2() {
        return test;
    }

    public void setTest2(String test2) {
        test=test2;
        this.test2 = test2;
    }
    
       public static void wydawnictwo() throws ClassNotFoundException, SQLException{
        w.clear();
          Statement s=Danych.getStatment();
          ResultSet rs2=s.executeQuery("SELECT * from wydawnictwo");
         while(rs2.next()){
             int id=rs2.getInt("id");
              String nazwa=rs2.getString("nazwa");
              String ulica=rs2.getString("ulica");
              String miasto=rs2.getString("miasto");
              String kod=rs2.getString("kod");
              w.add(new Wydawnictwo(id, nazwa, ulica, miasto, kod));
         }
         for(int n=0;n<w.size();n++){
             wyd.put( w.get(n).getNazwa(),Integer.toString(w.get(n).getId()));
             test=w.get(n).getNazwa();
         }
    }
   public static void autor() throws ClassNotFoundException, SQLException{
         a.clear();
          Statement s=Danych.getStatment();
          ResultSet rs2=s.executeQuery("SELECT * from autor");
         while(rs2.next()){
             int id=rs2.getInt("id");
              String imie=rs2.getString("imie");
              String nazwisko=rs2.getString("nazwisko");
              String url=rs2.getString("url");
              String opis=rs2.getString("opis");
              a.add(new Autor(id, imie, nazwisko, url, opis));
         }
         for(int n=0;n<a.size();n++){
             aut.put( a.get(n).getImie()+" "+a.get(n).getNaziwsko(),Integer.toString(a.get(n).getId()));
               test3=a.get(n).getImie()+" "+a.get(n).getNaziwsko();
         }
   }
   public String dodaj() throws SQLException{
        try {
            Statement s=Danych.getStatment();
            System.out.println("TEST2: "+test2);
            id_wydawnictwa=Integer.parseInt(test2);
            System.out.println("TEST4: "+test4);
            id_autora=Integer.parseInt(test4);
            int lol = s.executeUpdate("INSERT INTO ksiazka (tytul,url,liczba_stron,cena,ilosc,wydanie,id_autora,id_wydawnictwa) VALUES ('"+tytul+"','"+url+"','"+liczba_stron+"','"+cena+"','"+ilosc+"','"+wydanie+"','"+id_autora+"','"+id_wydawnictwa+"') ");
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(Rejestracja.class.getName()).log(Level.SEVERE, null, ex);
        }
        return "/panel";
   }
    public Map<String, String> getWyd() throws ClassNotFoundException, SQLException {
        wydawnictwo();
        return wyd;
    }

    public void setWyd(Map<String, String> wyd) {
        this.wyd = wyd;
    }

    public  Map<String, String> getAut() throws ClassNotFoundException, SQLException {
        autor();
        return aut;
    }

    public  void setAut(Map<String, String> aut) {
        KsiazkaD.aut = aut;
    }

    public String getTest4() {
        return test3;
    }

    public void setTest4(String test4) {
        test3=test4;
        this.test4 = test4;
    }

    public String getTytul() {
        return tytul;
    }

    public void setTytul(String tytul) {
        this.tytul = tytul;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public int getLiczba_stron() {
        return liczba_stron;
    }

    public void setLiczba_stron(int liczba_stron) {
        this.liczba_stron = liczba_stron;
    }

    public int getCena() {
        return cena;
    }

    public void setCena(int cena) {
        this.cena = cena;
    }

    public int getIlosc() {
        return ilosc;
    }

    public void setIlosc(int ilosc) {
        this.ilosc = ilosc;
    }

    public String getWydanie() {
        return wydanie;
    }

    public void setWydanie(String wydanie) {
        this.wydanie = wydanie;
    }

    public static String getTest3() {
        return test3;
    }

    public static void setTest3(String test3) {
        KsiazkaD.test3 = test3;
    }



    public static void setTratest2(String tratest2) {
        KsiazkaD.tratest2 = tratest2;
    }
    
   
}
