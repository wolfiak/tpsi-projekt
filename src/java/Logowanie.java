
import java.math.BigInteger;
import java.security.SecureRandom;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author pacio
 */
@ManagedBean(name="log")
@SessionScoped
public class Logowanie {
    private String email;
    private String haslo;
    private static Uzytkownik uz;
    private Uzytkownik uzo;
    private  String klucz;
    private SecureRandom random=new SecureRandom();
    
    
    public String logowanie() throws ClassNotFoundException, SQLException{
        Statement s=Danych.getStatment();
        ResultSet rs=s.executeQuery("SELECT * from uzytkownik WHERE email='"+email+"' AND password='"+haslo+"'");
        if(rs.next()){
              int id=rs.getInt("id");
             String imie=rs.getString("firstname");
             String naz=rs.getString("lastname");
             String mail=rs.getString("email");
             String ulica=rs.getString("ulica");
             String miasto=rs.getString("miasto");
             String kod=rs.getString("kod");  
             uz=new Uzytkownik(id,imie,naz,mail,ulica,miasto,kod);
             uzo=uz;
              if(imie!=null){
                   klucz=sesja();
                  s.executeUpdate("INSERT INTO sesja (klucz,id_klienta) VALUES ('"+klucz+"','"+id+"') ");
              }
             
        }
       
        return "/index";
       
    }

    public Uzytkownik getUzo() {
        return uzo;
    }

    public void setUzo(Uzytkownik uzo) {
        this.uzo = uzo;
    }

    public static Uzytkownik getUz() {
        return uz;
    }

    public void setUz(Uzytkownik uz) {
        this.uz = uz;
    }
    
    public String wyloguj(){
        klucz=null;
        uz=null;
        return "/index";
    }

    public  String getKlucz() {
        return klucz;
    }

    public void setKlucz(String klucz) {
        this.klucz = klucz;
    }
    
    private String sesja(){
        return new BigInteger(130,random).toString(32);
    }
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getHaslo() {
        return haslo;
    }

    public void setHaslo(String haslo) {
        this.haslo = haslo;
    }
    
    
}
