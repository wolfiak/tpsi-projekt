
import java.io.Serializable;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author pacio
 */
@ManagedBean(name="kup")
@SessionScoped
public class Kupteraz implements Serializable{
    private static int wybor;
    private Ksiazka[] tab=Baza.getKsiazka();
    private Ksiazka ten;
    private int ile;
    private String test="na ten moment nic";
    
    public void klik(){
        test="klik";
    }
    public String sprzedaz() throws SQLException{
        test="start";
         Statement s;
        try {
            s = Danych.getStatment();
            int idu=Logowanie.getUz().getId();
            int idk=tab[wybor-1].getId();
            int suma=tab[wybor-1].getCena()*ile;
            
             int lol = s.executeUpdate("INSERT INTO sprzedaz (id_klienta,id_ksiazki,ilosc,suma) VALUES ("+idu+","+idk+","+ile+","+suma+") ");
             
         
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(Kupteraz.class.getName()).log(Level.SEVERE, null, ex);
        }
           return "/index";
    }

    public String getTest() {
        return test;
    }

    public void setTest(String test) {
        this.test = test;
    }
    
    public int getIle() {
        return ile;
    }

    public void setIle(int ile) {
        this.ile = ile;
    }

    public Ksiazka[] getTab() {
        return tab;
    }

    public void setTab(Ksiazka[] tab) {
        this.tab = tab;
    }

    public Ksiazka getTen() {
        return tab[wybor-1];
    }

    public void setTen(Ksiazka ten) {
        this.ten = ten;
    }
    public static int getWybor() {
        return wybor;
    }

    public static void setWybor(int wybor) {
        Kupteraz.wybor = wybor;
        
    }
    
    
}
