
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author pacio
 */
@ManagedBean(name="ksiazkae")
@SessionScoped
public class KsiazkaE {
    private int id;
        private String tytul;
    private String url;
    private int liczba_stron;
    private int cena;
    private int ilosc;
   private String wydanie;
   private int id_autora;
   private int id_wydawnictwa;
    private Autor autor;
    private Wydawnictwo wydaw;
       public String dodaj() throws SQLException{
         try {
            Statement s=Danych.getStatment();
            id_wydawnictwa=Integer.parseInt(KsiazkaD.getTest());
            id_autora=Integer.parseInt(KsiazkaD.getTest3());
            s.execute("DELETE FROM ksiazka WHERE id="+id+"");
            int lol = s.executeUpdate("INSERT INTO ksiazka (id,tytul,url,liczba_stron,cena,ilosc,wydanie,id_autora,id_wydawnictwa) VALUES ('"+id+"','"+tytul+"','"+url+"','"+liczba_stron+"','"+cena+"','"+ilosc+"','"+wydanie+"','"+id_autora+"','"+id_wydawnictwa+"') ");
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(Rejestracja.class.getName()).log(Level.SEVERE, null, ex);
        }
        return "/panel";
    }
        public String odbierz(){
         Map<String, String> parameterMap = (Map<String, String>) FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
             int tmp = Integer.parseInt(parameterMap.get("id"));
             ListaKsiazka k= Panel.getK();
             Ksiazka tmpk=null;
             for(int n=0;n<k.size();n++){
                 if(k.get(n).getId()==tmp){
                     tmpk=k.get(n);
                 }
                 
             }
             id=tmpk.getId();
             tytul=tmpk.getTytul();
             url=tmpk.getUrl();
             liczba_stron=tmpk.getLiczba_stron();
             cena=tmpk.getCena();
             ilosc=tmpk.getIlosc();
             wydanie=tmpk.getWydanie();
             id_autora=tmpk.getAutor().getId();
             id_wydawnictwa=tmpk.getWydawnictwo().getId();
             wydaw=tmpk.getWydawnictwo();
             autor=tmpk.getAutor();
           
             return "/edycjaksi";
    }
        public String usun() throws ClassNotFoundException, SQLException{
              Map<String, String> parameterMap = (Map<String, String>) FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
             int tmp = Integer.parseInt(parameterMap.get("id"));
             Statement s=Danych.getStatment();
            s.execute("DELETE FROM ksiazka WHERE id="+tmp+"");
            
            return "ksiazkae";
            
        }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTytul() {
        return tytul;
    }

    public void setTytul(String tytul) {
        this.tytul = tytul;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public int getLiczba_stron() {
        return liczba_stron;
    }

    public void setLiczba_stron(int liczba_stron) {
        this.liczba_stron = liczba_stron;
    }

    public int getCena() {
        return cena;
    }

    public void setCena(int cena) {
        this.cena = cena;
    }

    public int getIlosc() {
        return ilosc;
    }

    public void setIlosc(int ilosc) {
        this.ilosc = ilosc;
    }

    public String getWydanie() {
        return wydanie;
    }

    public void setWydanie(String wydanie) {
        this.wydanie = wydanie;
    }

    public int getId_autora() {
        return id_autora;
    }

    public void setId_autora(int id_autora) {
        this.id_autora = id_autora;
    }

    public int getId_wydawnictwa() {
        return id_wydawnictwa;
    }

    public void setId_wydawnictwa(int id_wydawnictwa) {
        this.id_wydawnictwa = id_wydawnictwa;
    }

    public Autor getAutor() {
        return autor;
    }

    public void setAutor(Autor autor) {
        this.autor = autor;
    }

    public Wydawnictwo getWydaw() {
        return wydaw;
    }

    public void setWydaw(Wydawnictwo wydaw) {
        this.wydaw = wydaw;
    }
        
        
}
