
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author pacio
 */
@ManagedBean(name="aplikuj")
@SessionScoped
public class Aplikuj {
    private String email;
    private String haslo;
    private String imie;
    private String nazwisko;
    private String ulica;
    private String miasto;
    private String kod_pocztowy;
    
    
       public String rejestruj() throws SQLException{
        try {
            Statement s=Danych.getStatment();
            int lol = s.executeUpdate("INSERT INTO pracownik (firstname,lastname,email,password,ulica,miasto,kod) VALUES ('"+imie+"','"+nazwisko+"','"+email+"','"+haslo+"','"+ulica+"','"+miasto+"','"+kod_pocztowy+"') ");
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(Rejestracja.class.getName()).log(Level.SEVERE, null, ex);
        }
        return "/index";
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getHaslo() {
        return haslo;
    }

    public void setHaslo(String haslo) {
        this.haslo = haslo;
    }

    public String getImie() {
        return imie;
    }

    public void setImie(String imie) {
        this.imie = imie;
    }

    public String getNazwisko() {
        return nazwisko;
    }

    public void setNazwisko(String nazwisko) {
        this.nazwisko = nazwisko;
    }

    public String getUlica() {
        return ulica;
    }

    public void setUlica(String ulica) {
        this.ulica = ulica;
    }

    public String getMiasto() {
        return miasto;
    }

    public void setMiasto(String miasto) {
        this.miasto = miasto;
    }

    public String getKod_pocztowy() {
        return kod_pocztowy;
    }

    public void setKod_pocztowy(String kod_pocztowy) {
        this.kod_pocztowy = kod_pocztowy;
    }
       
       
       
}
