
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author pacio
 */
@ManagedBean(name="autord")
@SessionScoped
public class AutorD {
        private String imie;
        private String nazwisko;
        private String url;
        private String opis;
    
    
       public String dodaj() throws SQLException{
        try {
            Statement s=Danych.getStatment();
            int lol = s.executeUpdate("INSERT INTO autor (imie,nazwisko,url,opis) VALUES ('"+imie+"','"+nazwisko+"','"+url+"','"+opis+"') ");
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(Rejestracja.class.getName()).log(Level.SEVERE, null, ex);
        }
        return "/panel";
    }

    public String getImie() {
        return imie;
    }

    public void setImie(String imie) {
        this.imie = imie;
    }

    public String getNazwisko() {
        return nazwisko;
    }

    public void setNazwisko(String nazwisko) {
        this.nazwisko = nazwisko;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getOpis() {
        return opis;
    }

    public void setOpis(String opis) {
        this.opis = opis;
    }

   
       
}
