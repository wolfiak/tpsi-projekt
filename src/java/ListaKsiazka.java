
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author pacio
 */
public class ListaKsiazka {
      private List<Ksiazka> k=new ArrayList<Ksiazka>();
      
      
      public   void add(Ksiazka ks){
          k.add(ks);
      }
      public  Ksiazka get(int index){
          return k.get(index);
      }
      public void clear(){
          k.clear();
      }
      public Ksiazka find(int id){
          Ksiazka ko = null;
          for(int n=0;n<k.size();n++){
              if(k.get(n).getId()==id){
                  ko=k.get(n);
              }
          }
          return ko;
      }
      public int size(){
         return k.size();
      }

    public List<Ksiazka> getK() {
        return k;
    }

    public void setK(List<Ksiazka> k) {
        this.k = k;
    }
      
}
