
import java.io.Serializable;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author pacio
 */
@ManagedBean(name="baza")
@SessionScoped
public class Baza implements Serializable{
    private Statement s;
    private Connection c;
    private String url="jdbc:mysql://localhost:3306/sys";
    private String username = "root";
    private String password = "haslo";
    private String p="Nie polaczono";
    private static ListaKsiazka k= new ListaKsiazka();
    private static ListaAutor a= new ListaAutor();
    private ListaWydawnictwo w=new ListaWydawnictwo();
    private static Ksiazka[] tab;
    private static Ksiazka[] taba;
    private static Autor[] autorowie;
    private int action;
    private int actiona;
    private  Ksiazka opisgk;
    private Autor opisaa;
    private int licznik=0;
    public void polacz() throws ClassNotFoundException{
       
        try {
            Class.forName("com.mysql.jdbc.Driver");
             c = DriverManager.getConnection(url,username,password);
             s=c.createStatement();
             ksiazki();
             p="polaczono";
             c.close();
        } catch (SQLException ex) {
            Logger.getLogger(Baza.class.getName()).log(Level.SEVERE, null, ex);
        }
       
        

    }


    public String opis(){
       Map<String, String> parameterMap = (Map<String, String>) FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
       action = Integer.parseInt(parameterMap.get("id"));
       Opis.setWybor(action);
       return "/opis";
    }
    public String autor(){
         Map<String, String> parameterMap = (Map<String, String>) FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
           actiona = Integer.parseInt(parameterMap.get("id"));
           AutorW.setWybor(actiona);
             return "/autor";
    }
    public static Ksiazka[] getKsiazka(){
        return tab;
    }
    public int getActiona() {
        return actiona;
    }

    public void setActiona(int actiona) {
        this.actiona = actiona;
    }
    
    public void opisg(){
        opisgk=k.find(action);
        
    }
    public void opisa(){
        opisaa=a.find(actiona);
        taba=listaA(actiona);
    }

    public static Autor[] getAutorowie() {
       Autor[] tabka=new Autor[a.size()];
        for(int n=0;n<a.size();n++){
            List<Ksiazka> ks=new ArrayList<>();
            for(int m=0;m<k.size();m++){
                if(a.get(n).getId()==k.get(m).getAutor().getId()){
                    ks.add(k.get(m));
                }
            }
            Ksiazka[] tks=new Ksiazka[ks.size()];
            for(int m=0;m<ks.size();m++){
                tks[m]=ks.get(m);
            }
             a.get(n).setKsiazka(tks);
        }
        Autor[] atab=new Autor[a.size()];
        for(int n=0;n<a.size();n++){
            atab[n]=a.get(n);
        }
        return atab;
    }

    public static void setAutorowie(Autor[] autorowie) {
        Baza.autorowie = autorowie;
    }
   
    public Ksiazka[] listaA(int id){
        List<Ksiazka> l=new ArrayList<Ksiazka>();
        for(int n=0;n<k.size();n++){
            if(k.get(n).getAutor().getId()==id){
                l.add(k.get(n));
            }
        }
        Ksiazka[] tabo=new Ksiazka[l.size()];
        for(int n=0;n<k.size();n++){
            tabo[n]=l.get(n);
        }
        return tabo;
    }
    public int getAction() {
        return action;
    }

    public void setAction(int action) {
        this.action = action;
    }

    public  Ksiazka getOpisgk() {
        return opisgk;
    }

    public void setOpisgk(Ksiazka opisgk) {
        this.opisgk = opisgk;
    }

    public Autor getOpisaa() {
        return opisaa;
    }

    public void setOpisaa(Autor opisaa) {
        this.opisaa = opisaa;
    }
    
    public void ksiazki() throws SQLException{
        ResultSet rs1=s.executeQuery("SELECT * from autor");
        while(rs1.next()){
             int id=rs1.getInt("id");
            String imie=rs1.getString("imie");
            String nazwisko=rs1.getString("nazwisko");
             String url=rs1.getString("url");
              String opis=rs1.getString("opis");
             a.add(new Autor(id,imie,nazwisko,url,opis));
        }
         ResultSet rs2=s.executeQuery("SELECT * from wydawnictwo");
         while(rs2.next()){
             int id=rs2.getInt("id");
              String nazwa=rs2.getString("nazwa");
              String ulica=rs2.getString("ulica");
              String miasto=rs2.getString("miasto");
              String kod=rs2.getString("kod");
              w.add(new Wydawnictwo(id, nazwa, ulica, miasto, kod));
         }
        ResultSet rs=s.executeQuery("SELECT * from ksiazka");
        k.clear();
        licznik=0;
        while(rs.next()){
            licznik++;
            int id=rs.getInt("id");
            String tytul=rs.getString("tytul");
            String url=rs.getString("url");
            int liczba_stron=rs.getInt("liczba_stron");
            int id_autora=rs.getInt("id_autora");
            int cena=rs.getInt("cena");
            int ilosc=rs.getInt("ilosc");
            String wydanie=rs.getString("wydanie");
            int id_wydawnictwa=rs.getInt("id_wydawnictwa");
            Autor tmpa=null;
            for(int n=0;n<a.size();n++){
                if(a.get(n).getId()==id_autora){
                    tmpa=a.get(n);
                }
            }
            Wydawnictwo tmpw=null;
            for(int n=0;n<w.size();n++){
                if(w.get(n).getId()==id_wydawnictwa){
                    tmpw=w.get(n);
                }
            }
            k.add(new Ksiazka(id,tytul,url,liczba_stron,tmpa,cena,ilosc,wydanie,tmpw));
        }
        tab=new Ksiazka[k.size()];
        for(int n=0;n<k.size();n++){
            tab[n]=k.get(n);
        }
    }

    public int getLicznik() {
        return licznik;
    }

    public void setLicznik(int licznik) {
        this.licznik = licznik;
    }

    public static Ksiazka[] getTaba() {
        return taba;
    }

    public void setTaba(Ksiazka[] taba) {
        this.taba = taba;
    }

    public Ksiazka[] getTab() {
        return tab;
    }

    public void setTab(Ksiazka[] tab) {
        this.tab = tab;
    }

    public String getP() {
        return p;
    }

    public void setP(String p) {
        this.p = p;
    }
    
    public void koniec(){
        try {
            c.close();
            p="NOPE";
        } catch (SQLException ex) {
            Logger.getLogger(Baza.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    public String kuptreraz(){
           Map<String, String> parameterMap = (Map<String, String>) FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
             int tmp = Integer.parseInt(parameterMap.get("id"));
             Kupteraz.setWybor(tmp);
             
             return "/kupteraz";
    }
}
