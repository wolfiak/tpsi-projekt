
import java.util.List;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author pacio
 */
public class Autor {
    private int id;
    private String imie;
    private String naziwsko;
    private String url;
    private String opis;
    private Ksiazka[] ksiazka;

    public Ksiazka[] getKsiazka() {
        return ksiazka;
    }

    public void setKsiazka(Ksiazka[] ksiazka) {
        this.ksiazka = ksiazka;
    }

    public Autor(int id, String imie, String naziwsko, String url, String opis) {
        this.id = id;
        this.imie = imie;
        this.naziwsko = naziwsko;
        this.url = url;
        this.opis = opis;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getImie() {
        return imie;
    }

    public void setImie(String imie) {
        this.imie = imie;
    }

    public String getNaziwsko() {
        return naziwsko;
    }

    public void setNaziwsko(String naziwsko) {
        this.naziwsko = naziwsko;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getOpis() {
        return opis;
    }

    public void setOpis(String opis) {
        this.opis = opis;
    }
    
    
}
