/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author pacio
 */
public class Sprzedaz {
    private Ksiazka ksiazka;
    private Uzytkownik uz;
    private int id;
    
    

    public Uzytkownik getUz() {
        return uz;
    }

    public void setUz(Uzytkownik uz) {
        this.uz = uz;
    }
    private int ilosc;
    private int suma;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Sprzedaz(Ksiazka ksiazka, int ilosc, int suma) {
        this.ksiazka = ksiazka;
        this.ilosc = ilosc;
        this.suma = suma;
    }

    public Ksiazka getKsiazka() {
        return ksiazka;
    }

    public void setKsiazka(Ksiazka ksiazka) {
        this.ksiazka = ksiazka;
    }

    public int getIlosc() {
        return ilosc;
    }

    public void setIlosc(int ilosc) {
        this.ilosc = ilosc;
    }

    public int getSuma() {
        return suma;
    }

    public void setSuma(int suma) {
        this.suma = suma;
    }
    
    
}
