
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author pacio
 */
@ManagedBean(name="opis")
@SessionScoped
public class Opis {
    private static int wybor;
    private  Ksiazka opisgk;
    private Ksiazka[] tab=Baza.getKsiazka();
    public static int getWybor() {
        return wybor;
    }

    public static void setWybor(int wybor) {
        Opis.wybor = wybor;
    }

    public Ksiazka getOpisgk() {
        Ksiazka k=null;
        for(int n=0;n<tab.length;n++){
            if(tab[n].getId()==wybor){
                k=tab[n];
            }
        }
        return k;
    }

    public void setOpisgk(Ksiazka opisgk) {
        this.opisgk = opisgk;
    }
    
    
}
