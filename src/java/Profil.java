
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author pacio
 */
@ManagedBean(name="profil")
@SessionScoped
public class Profil {
    private static ListaSprzedaz lista=new ListaSprzedaz();
    private static List<Sprzedaz> sp;
    private static String test="";
    
    public Profil(){
        sp=(List<Sprzedaz>) getLista().getK();
    }
   
    public static void zakupy() throws SQLException{
        try {
            lista.clear();
             Ksiazka[] tab=Baza.getKsiazka();
            Statement s=Danych.getStatment();
            ResultSet rs=s.executeQuery("SELECT * from sprzedaz WHERE id_klienta='"+Logowanie.getUz().getId()+"' ");
            while(rs.next()){
                  int idk=rs.getInt("id_ksiazki");
                   int ilosc=rs.getInt("ilosc");
                   int suma=rs.getInt("suma");
                   Ksiazka k = null;
                  
                   for(int n=0;n<Baza.getKsiazka().length;n++){
                       if(tab[n].getId()==idk){
                           k=tab[n];
                       }
                   }
                  lista.add(new Sprzedaz(k, ilosc, suma));
                  test="Poszlo";
                  
            }
            sp=(List<Sprzedaz>) getLista().getK();
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(Profil.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public  static ListaSprzedaz getLista() {
        return lista;
    }

    public void setLista(ListaSprzedaz lista) {
        this.lista = lista;
    }

    public List<Sprzedaz> getSp() {
        return sp;
    }

    public void setSp(List<Sprzedaz> sp) {
        this.sp = sp;
    }

    public String getTest() {
        return test;
    }

    public void setTest(String test) {
        this.test = test;
    }
    
}
