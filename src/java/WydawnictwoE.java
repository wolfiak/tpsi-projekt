
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author pacio
 */
@ManagedBean(name="wydawnictwoe")
@SessionScoped
public class WydawnictwoE {
    private int id;
    private String nazwa;
    private String ulica;
    private String miasto;
    private String kod;
    public String dodaj() throws SQLException{
         try {
            Statement s=Danych.getStatment();
            s.execute("DELETE FROM wydawnictwo WHERE id="+id+"");
            int lol = s.executeUpdate("INSERT INTO wydawnictwo (id,nazwa,ulica,miasto,kod) VALUES ('"+id+"','"+nazwa+"','"+ulica+"','"+miasto+"','"+kod+"') ");
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(Rejestracja.class.getName()).log(Level.SEVERE, null, ex);
        }
        return "/panel";
    }
    public String odbierz(){
         Map<String, String> parameterMap = (Map<String, String>) FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
             int tmp = Integer.parseInt(parameterMap.get("id"));
             ListaWydawnictwo w= Panel.getW();
             Wydawnictwo tmpw=null;
             for(int n=0;n<w.size();n++){
                 if(w.get(n).getId()==tmp){
                     tmpw=w.get(n);
                 }
                 
             }
             id=tmpw.getId();
             nazwa=tmpw.getNazwa();
             ulica=tmpw.getUlica();
             miasto=tmpw.getMiasto();
             kod=tmpw.getKod();
             System.out.println("nazwa: "+nazwa);
             return "/edycjaaut";
    }
    public String usun() throws ClassNotFoundException, SQLException{
         Map<String, String> parameterMap = (Map<String, String>) FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
         int tmp = Integer.parseInt(parameterMap.get("id"));
         Statement s=Danych.getStatment();
          s.execute("DELETE FROM wydawnictwo WHERE id="+tmp+"");
          
          return "wydawnictwoe";
    }
    public String getNazwa() {
        return nazwa;
    }

    public void setNazwa(String nazwa) {
        this.nazwa = nazwa;
    }

    public String getUlica() {
        return ulica;
    }

    public void setUlica(String ulica) {
        this.ulica = ulica;
    }

    public String getMiasto() {
        return miasto;
    }

    public void setMiasto(String miasto) {
        this.miasto = miasto;
    }

    public String getKod() {
        return kod;
    }

    public void setKod(String kod) {
        this.kod = kod;
    }
    

}
