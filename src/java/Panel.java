
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author pacio
 */
@ManagedBean(name="panel")
@SessionScoped
public class Panel {
    private static ListaWydawnictwo w=new ListaWydawnictwo();
     private static ListaAutor a=new ListaAutor();
     private ListaSprzedaz sp=new ListaSprzedaz();
    private List<Wydawnictwo> lista;
    private ListaUzytkownikow u=new ListaUzytkownikow();
    private static ListaKsiazka k=new ListaKsiazka();
    private List<Autor> listaA;
    private List<Sprzedaz> listaSp;
    private List<Ksiazka> listak;
    public void wydawnictwo() throws ClassNotFoundException, SQLException{
        w.clear();
          Statement s=Danych.getStatment();
          ResultSet rs2=s.executeQuery("SELECT * from wydawnictwo");
         while(rs2.next()){
             int id=rs2.getInt("id");
              String nazwa=rs2.getString("nazwa");
              String ulica=rs2.getString("ulica");
              String miasto=rs2.getString("miasto");
              String kod=rs2.getString("kod");
              w.add(new Wydawnictwo(id, nazwa, ulica, miasto, kod));
         }
    }
     public void uzytkownik() throws ClassNotFoundException, SQLException{
        u.clear();
          Statement s=Danych.getStatment();
          ResultSet rs2=s.executeQuery("SELECT * from uzytkownik");
         while(rs2.next()){
             int id=rs2.getInt("id");
              String imie=rs2.getString("firstname");
              String nazwisko=rs2.getString("lastname");
                String email=rs2.getString("email");
              String haslo=rs2.getString("password");
               String ulica=rs2.getString("ulica");
                String miasto=rs2.getString("miasto");
               String kod=rs2.getString("kod");
              u.add(new Uzytkownik(id, imie, nazwisko, email, ulica, miasto, kod));
         }
    }
    public void autor() throws ClassNotFoundException, SQLException{
        a.clear();
          Statement s=Danych.getStatment();
          ResultSet rs2=s.executeQuery("SELECT * from autor");
         while(rs2.next()){
             int id=rs2.getInt("id");
              String nazwa=rs2.getString("imie");
              String ulica=rs2.getString("nazwisko");
                String url=rs2.getString("url");
              String miasto=rs2.getString("opis");
              a.add(new Autor(id, nazwa, ulica,url, miasto));
         }
    }
         public void ksiazka() throws ClassNotFoundException, SQLException{
             autor();
             wydawnictwo();
          k.clear();
          Statement s=Danych.getStatment();
          ResultSet rs2=s.executeQuery("SELECT * from ksiazka");
         while(rs2.next()){
             int id=rs2.getInt("id");
              String tytul=rs2.getString("tytul");
              String url=rs2.getString("url");
               int liczba_stron=rs2.getInt("liczba_stron");
               int cena=rs2.getInt("cena");
               int ilosc=rs2.getInt("ilosc");
                String wydanie=rs2.getString("wydanie");
                int id_autora=rs2.getInt("id_autora");
                 System.out.println("Autor ID: "+id_autora);
                Autor aa = null;
                for(int n=0;n<a.size();n++){
                    if(id_autora==a.get(n).getId()){
                        aa=a.get(n);
                    }
                }
                Wydawnictwo ww=null;
                 int id_wydawnictwa=rs2.getInt("id_wydawnictwa");
                for(int n=0;n<w.size();n++){
                    if(id_wydawnictwa==w.get(n).getId()){
                        ww=w.get(n);
                    }
                }
               System.out.println("Autor: "+aa.getImie()+" "+aa.getNaziwsko());
              k.add(new Ksiazka(id, tytul, url, liczba_stron, aa, cena, ilosc, wydanie, ww));
         }
    }
    public void sprzedaz() throws ClassNotFoundException, SQLException{
        uzytkownik();
        ksiazka();
            sp.clear();
          Statement s=Danych.getStatment();
          ResultSet rs2=s.executeQuery("SELECT * from sprzedaz");
         while(rs2.next()){
             int id=rs2.getInt("id");
              int idu=rs2.getInt("id_klienta");
              Uzytkownik tmp = null;
           for(int n=0;n<u.size();n++){
               if(u.get(n).getId()== idu){
                   tmp=u.get(n);
               }
           }
              int idk=rs2.getInt("id_ksiazki");
                Ksiazka tmp2 = null;
           for(int n=0;n<k.size();n++){
               if(k.get(n).getId()== idk){
                   tmp2=k.get(n);
               }
               
           }
           int ilosc=rs2.getInt("ilosc");
            int suma=rs2.getInt("suma");
            sp.add(new Sprzedaz(tmp2, ilosc, suma));
            sp.get(sp.size()-1).setUz(tmp);
            sp.get(sp.size()-1).setId(id);
         }
    }

    public List<Sprzedaz> getListaSp() throws ClassNotFoundException, SQLException {
        sprzedaz();
        return sp.getK();
    }

    public void setListaSp(List<Sprzedaz> listaSp) {
        this.listaSp = listaSp;
    }

    public List<Autor> getListaA() throws ClassNotFoundException, SQLException {
       autor();
       return a.getK();
    }

    public void setListaA(List<Autor> listaA) {
        this.listaA = listaA;
    }


    public List<Wydawnictwo> getLista() throws ClassNotFoundException, SQLException {
        wydawnictwo();
    
        return w.getK();
    }

    public void setLista(List<Wydawnictwo> lista) {
        this.lista = lista;
    }

    public static ListaWydawnictwo getW() {
        return w;
    }

    public static void setW(ListaWydawnictwo w) {
        Panel.w = w;
    }

    public static ListaAutor getA() {
        return a;
    }

    public static void setA(ListaAutor a) {
        Panel.a = a;
    }

    public static ListaKsiazka getK() {
        return k;
    }

    public static void setK(ListaKsiazka k) {
        Panel.k = k;
    }

    public List<Ksiazka> getListak() throws ClassNotFoundException, SQLException {
        ksiazka();
       return k.getK();
    }

    public void setListak(List<Ksiazka> listak) {
        
        this.listak = listak;
    }
    
}
