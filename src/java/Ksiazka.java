/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author pacio
 */
public class Ksiazka {
    private int id;
    private String tytul;
    private String url;
    private int liczba_stron;
    private Autor autor;
    private int cena;
    private int ilosc;
    private String wydanie;
    private Wydawnictwo wydawnictwo;

    public Ksiazka(int id, String tytul, String url, int liczba_stron, Autor autor, int cena, int ilosc, String wydanie, Wydawnictwo wydawnictwo) {
        this.id = id;
        this.tytul = tytul;
        this.url = url;
        this.liczba_stron = liczba_stron;
        this.autor = autor;
        this.cena = cena;
        this.ilosc = ilosc;
        this.wydanie = wydanie;
        this.wydawnictwo = wydawnictwo;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTytul() {
        return tytul;
    }

    public void setTytul(String tytul) {
        this.tytul = tytul;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public int getLiczba_stron() {
        return liczba_stron;
    }

    public void setLiczba_stron(int liczba_stron) {
        this.liczba_stron = liczba_stron;
    }

    public Autor getAutor() {
        return autor;
    }

    public void setAutor(Autor autor) {
        this.autor = autor;
    }

    public int getCena() {
        return cena;
    }

    public void setCena(int cena) {
        this.cena = cena;
    }

    public int getIlosc() {
        return ilosc;
    }

    public void setIlosc(int ilosc) {
        this.ilosc = ilosc;
    }

    public String getWydanie() {
        return wydanie;
    }

    public void setWydanie(String wydanie) {
        this.wydanie = wydanie;
    }

    public Wydawnictwo getWydawnictwo() {
        return wydawnictwo;
    }

    public void setWydawnictwo(Wydawnictwo wydawnictwo) {
        this.wydawnictwo = wydawnictwo;
    }

    
}
