
import java.sql.SQLException;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author pacio
 */
@ManagedBean(name="route")
@SessionScoped
public class Route {
    
    public String rej(){
        return "/rejestracja";
    }
    public String wydawnictwoD(){
        return "/wydawnictwod";
    }
    public String wydawnictwoE(){
        return "/wydawnictwoe";
    }
    public String autorD(){
        return "autord";
    }
    public String autorE(){
        return "autore";
    }
    public String ksiazkaD() throws ClassNotFoundException, SQLException{
        KsiazkaD.wydawnictwo();
        return "/ksiazkad";
    }
    public String ksiazkaE(){
        return "/ksiazkae";
    }
    public String log(){
        return "/logowanie";
    }
    public String profil() throws SQLException{
        Profil.zakupy();
        return "/profil";
    }
    public String aplikuj(){
        return "/aplikuj";
    }
    public String logp(){
        return "/logowaniep";
    }

}
